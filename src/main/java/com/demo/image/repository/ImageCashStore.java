package com.demo.image.repository;

import com.demo.image.model.image.ImageDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ImageCashStore {

    private List<ImageDto> cashImages = new ArrayList<>();

    public void addToStore(List<ImageDto> imagesToAdd) {
        cashImages.addAll(imagesToAdd);
    }

    public void clearStore() {
        cashImages = new ArrayList<>();
    }

    public int getImagesSize() {
        return cashImages.size();
    }

    public List<ImageDto> findAllBySearchTerm(String searchTerm) {
        return cashImages.parallelStream()
                .filter(e->containsTerm(searchTerm).test(e))
                .collect(Collectors.toList());
    }

    private Predicate<ImageDto> containsTerm(String searchTerm) {
        return (imageDto) -> {
            return textContainsWord(" ", searchTerm).test(imageDto.getAuthor())
                    || textContainsWord(" ", searchTerm).test(imageDto.getCamera())
                    || textContainsWord("#", searchTerm).test(imageDto.getTags());
        };
    }

    private Predicate<String> textContainsWord(String delimeter, String searchTerm) {
        return text -> {
            if(Strings.isBlank(text)) {
                return false;
            }
            return Arrays.stream(text.split(delimeter))
                    .map(String::trim)
                    .anyMatch(element->element.equals(searchTerm));
        };
    }

}
