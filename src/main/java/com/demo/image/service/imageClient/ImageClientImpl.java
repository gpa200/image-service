package com.demo.image.service.imageClient;

import com.demo.image.model.image.ImagePageDto;
import com.demo.image.model.image.ImageDto;
import com.demo.image.service.auth.AuthorizationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class ImageClientImpl implements ImageClient {

    private final AuthorizationService authorizationService;
    private RestTemplate restTemplate = new RestTemplate();

    @Value("${baseImageURL}")
    private String baseImageURL;
    private String IMAGES_PATH = "/images";

    @Override
    public ImagePageDto fetchPage(int page) {
        try {
            return fetchPage(page, false);
        } catch (Exception e) {
            return fetchPage(page, true);
        }
    }

    private ImagePageDto fetchPage(int page, boolean refreshToken) {
        HttpHeaders headers = buildAuthHttpHeader(refreshToken);
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<ImagePageDto> imagePageResponse = restTemplate.exchange(baseImageURL + IMAGES_PATH + "?page="+page, HttpMethod.GET, entity, ImagePageDto.class);
        return imagePageResponse.getBody();
    }

    @Override
    public ImageDto getOne(String imageId) {
        try {
            return getOne(imageId, false);
        } catch (Exception e) {
            return getOne(imageId, true);
        }
    }

    public ImageDto getOne(String imageId, boolean refreshToken) {
        HttpHeaders headers = buildAuthHttpHeader(false);
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<ImageDto> imagePageResponse = restTemplate.exchange(baseImageURL + IMAGES_PATH + "/" + imageId, HttpMethod.GET, entity, ImageDto.class);
        return imagePageResponse.getBody();
    }

    private HttpHeaders buildAuthHttpHeader(boolean refreshToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(refreshToken ? authorizationService.getNewToken() : authorizationService.getCashedToken());
        return headers;
    }
}
