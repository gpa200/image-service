package com.demo.image.service.imageClient;

import com.demo.image.model.image.ImagePageDto;
import com.demo.image.model.image.ImageDto;

public interface ImageClient {

    ImagePageDto fetchPage(int page);

    ImageDto getOne(String imageId);

}
