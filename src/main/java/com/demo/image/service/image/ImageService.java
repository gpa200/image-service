package com.demo.image.service.image;

import com.demo.image.model.image.ImageDto;
import com.demo.image.model.image.ImagePageDto;

import java.util.List;

public interface ImageService {

    ImagePageDto fetchPage(int page);

    ImageDto getOne(String imageId);

    List<ImageDto> findAllBySearchTerm(String searchTerm);

}
