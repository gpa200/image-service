package com.demo.image.service.image;

import com.demo.image.model.image.ImageDto;
import com.demo.image.model.image.ImagePageDto;
import com.demo.image.repository.ImageCashStore;
import com.demo.image.service.imageClient.ImageClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

    private final ImageClient imageClient;
    private final ImageCashStore imageCashStore;

    @Override
    public ImagePageDto fetchPage(int page) {
        return imageClient.fetchPage(page);
    }

    @Override
    public ImageDto getOne(String imageId) {
        return imageClient.getOne(imageId);
    }

    @Override
    public List<ImageDto> findAllBySearchTerm(String searchTerm) {
        return imageCashStore.findAllBySearchTerm(searchTerm);
    }
}
