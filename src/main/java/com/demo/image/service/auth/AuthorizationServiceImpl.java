package com.demo.image.service.auth;

import com.demo.image.model.auth.AuthTokenDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthorizationServiceImpl implements AuthorizationService {

    @Value("${apiKey}")
    private String apiKey;

    @Value("${baseImageURL}")
    private String baseImageURL;

    private String token;

    @Override
    public String getNewToken() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Map<String, String>> request = new HttpEntity<>(Map.of("apiKey", apiKey));
        ResponseEntity<AuthTokenDto> response = restTemplate
                .exchange(baseImageURL + "/auth", HttpMethod.POST, request, AuthTokenDto.class);
        this.token = response.getBody().getToken();
        return this.token;
    }

    @Override
    public String getCashedToken() {
        return Optional.ofNullable(this.token).orElse(getNewToken());
    }
}
