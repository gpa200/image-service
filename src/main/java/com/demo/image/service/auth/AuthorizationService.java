package com.demo.image.service.auth;

public interface AuthorizationService {
    String getNewToken();

    String getCashedToken();
}
