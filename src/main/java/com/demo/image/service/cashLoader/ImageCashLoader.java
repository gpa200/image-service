package com.demo.image.service.cashLoader;

import com.demo.image.model.image.ImageDto;

import java.util.List;

public interface ImageCashLoader {

    List<ImageDto> findAllBy(String searchTerm);


}
