package com.demo.image.service.cashLoader;

import com.demo.image.model.image.ImageDto;
import com.demo.image.model.image.ImagePageDto;
import com.demo.image.model.image.ShortImageDto;
import com.demo.image.repository.ImageCashStore;
import com.demo.image.service.imageClient.ImageClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ImageCashLoaderImpl {

    private final ImageClient imageClient;

    private final ImageCashStore imageCashStore;

    @PostConstruct
    public void loadImages() {
        log.info("Load all images to local cash");
        int page = 1;
        boolean hasNextPage = true;
        imageCashStore.clearStore();
        while(hasNextPage) {
            ImagePageDto imagePage = imageClient.fetchPage(page++);
            log.info("Loaded page {} of {}", imagePage.getPage(), imagePage.getPageCount());
            hasNextPage = imagePage.getHasMore();
            imageCashStore.addToStore(getFullImagesFor(imagePage.getPictures()));
        }
        log.info("Loaded {} to images cash ", imageCashStore.getImagesSize());
    }

    private List<ImageDto> getFullImagesFor(List<ShortImageDto> images) {
        if(images == null || images.isEmpty()) {
            return Collections.emptyList();
        }
        return images.parallelStream()
                .map(img->getFullImage(img))
                .collect(Collectors.toList());
    }

    private ImageDto getFullImage(ShortImageDto shortImage) {
        try {
            ImageDto imageDto = imageClient.getOne(shortImage.getId());
            log.debug("Loaded full image for id {}", shortImage.getId());
            return imageDto;
        } catch (Exception e) {
            log.error("Error to get full image for {}", shortImage.getId(), e);
            return ImageDto.builder()
                    .id(shortImage.getId())
                    .croppedPicture(shortImage.getCroppedPicture())
                    .build();
        }
    }

    @Scheduled(cron = "${reloadImageCashCron}")
    public void reloadImages() {
        log.info("Refresh images cash");
        this.loadImages();
    }
}
