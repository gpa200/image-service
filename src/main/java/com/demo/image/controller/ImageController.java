package com.demo.image.controller;

import com.demo.image.model.image.ImageDto;
import com.demo.image.model.image.ImagePageDto;
import com.demo.image.service.image.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/images")
public class ImageController {

    private final ImageService imageService;

    @GetMapping
    public ImagePageDto getPage(@RequestParam(name = "page") Integer page) {
        return imageService.fetchPage(page);
    }

    @GetMapping(value = "/{imageId}")
    public ImageDto getOne(@PathVariable(name = "imageId") String imageId) {
        return imageService.getOne(imageId);
    }

    @GetMapping(value = "/search/{searchTerm}")
    public List<ImageDto> findAllBySearchTerm(@PathVariable(name = "searchTerm") String searchTerm) {
        return imageService.findAllBySearchTerm(searchTerm);
    }
}
