package com.demo.image.model.auth;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"token"})
public class AuthTokenDto {
    private Boolean auth;
    private String token;
}
