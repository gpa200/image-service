package com.demo.image.model.image;

import lombok.Data;

import java.util.List;

@Data
public class ImagePageDto {
    private List<ShortImageDto> pictures;
    private Integer page;
    private Integer pageCount;
    private Boolean hasMore;
}
