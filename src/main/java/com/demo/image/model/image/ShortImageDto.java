package com.demo.image.model.image;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class ShortImageDto {
    private String id;
    @JsonAlias("cropped_picture")
    private String croppedPicture;
}
