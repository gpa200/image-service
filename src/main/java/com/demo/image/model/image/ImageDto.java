package com.demo.image.model.image;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ImageDto {

    private String id;
    @JsonAlias("cropped_picture")
    private String croppedPicture;
    private String author;
    private String camera;
    private String tags;
    @JsonAlias("full_picture")
    private String fullPicture;
}
